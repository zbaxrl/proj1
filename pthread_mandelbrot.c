#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <getopt.h>
#include <pthread.h>
// 加载 png 图像输出库
#include "svpng.inc"
// 宏定义
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
// 最小工作块粒度，太小会导致严重的抢占开销
#define WORK_QUANTUM 10


void usage() {
  puts("pthread_mandelbrot -t threads -w width -h height");
  puts("pthread_mandelbrot -?");
  return;
}

struct workblock {
  int *spin;
  int width, height;
  int *atomic_current_width;
  float *frame;
};

float mandelbrot(double x, double y) {
    double a = x, b = y;
    for(int i = 0; i < 128; ++i) {
        double c = a * a - b * b + x, d = 2 * a * b + y;
        a = c, b = d;
        if(a * a + b * b > 4) return 1 - i / 128.0;
    }
    return 0;
}

void * working_thread(void *arg_ptr) {
  struct workblock *work = (struct workblock *) arg_ptr;
  volatile int *atomic_current_width = work->atomic_current_width;

  // 使用 GCC 内置原语在 work->spin 上自旋，等待主线程发出信号；保证多个线程同时运行
  int spin = 0;
  while ((spin = __atomic_load_n(work->spin, __ATOMIC_ACQUIRE)) == 0);

  // 使用 GCC 内置的 FAA 原语抢占一个计算任务
  int finish = 0;
  while (!finish) {
    int mywidth = __atomic_fetch_add(atomic_current_width, WORK_QUANTUM, __ATOMIC_RELAXED);
    // 此时我们需要计算的范围是 width [mywidth, mywidht + WORK_QUANTUM)
  
    if (mywidth > work->width) {
      finish = 1;
      continue;
    }

    int len = MIN(work->width - mywidth, WORK_QUANTUM);
    for (int i = 0; i < len; i++) {
      int x = i + mywidth;
      for (int y = 0; y < work->height; y++) {
        work->frame[y * work->width + x] =
          mandelbrot(x / ((float) work->width) * 3 - 2, y / ((float) work->height) * 2 - 1);
      }
    }
  }

  return NULL;
}

int main(int argc, char *const argv[]) {
  int opt;

  int threads = -1, width = -1, height = -1;

  // 读取命令行参数
  while((opt = getopt(argc, argv, "t:w:h:?")) != -1) {
    switch (opt) {
    case 't':
      sscanf(optarg, "%d", &threads);
      break;
    case 'w':
      sscanf(optarg, "%d", &width);
      break;
    case 'h':
      sscanf(optarg, "%d", &height);
      break;
    case '?':
    default:
      usage();
      exit(-1);
      break;
    }
  }

  // 如果参数不正确，展示命令行使用方法，退出
  if (threads == -1 || width == -1 || height == -1) {
    usage();
    exit(-1);
  }

  // 输出当前配置参数
  printf("Use %d threads for %dx%d image\n", threads, width, height);

  // frame 表示最终要输出的图片
  float *frame = malloc(width * height * sizeof(float));
  // spin 是自旋变量，置 1 时子线程开始计算
  int spin = 0;
  // atomic_current_width 是进行动态抢占式任务分配的变量
  int atomic_current_width = 0;

  // 装填工作信息，之后传给子进程
  struct workblock work = {
    .spin = &spin,
    .width = width,
    .height = height,
    .atomic_current_width = &atomic_current_width,
    .frame = frame
  };

  // 创建子进程
  // 此时子进程已经开始执行，但还未开始实际计算，需要等到 spin 释放
  // 这么做的好处是：尽可能让所有线程同时开始计算
  pthread_t *threads_struct = malloc(threads * sizeof(pthread_t));
  for (int i = 0; i < threads; i++) {
    pthread_create(&threads_struct[i], NULL, working_thread, &work);
  }

  // 使用 GCC 内置原语释放 spin，让子线程开始计算
  struct timeval begin;
  gettimeofday(&begin, NULL);
  __atomic_store_n(&spin, 1, __ATOMIC_RELEASE);

  // 等待各个工作线程结束
  for (int i = 0; i < threads; i++) {
    pthread_join(threads_struct[i], NULL);
  }
  struct timeval end;
  gettimeofday(&end, NULL);

  // 计算时间并输出
  double begin_ms = (double) begin.tv_sec * 1000 + (double) begin.tv_usec / 1000;
  double end_ms = (double) end.tv_sec * 1000 + (double) end.tv_usec / 1000;
  printf("Time elapsed %lf milliseconds.\n", end_ms - begin_ms);

  // 输出图片
  unsigned char *img = malloc(width * height * 3);
  for (int j = 0; j < height; j++) {
    for (int i = 0; i < width; i++) {
      img[j * width * 3 + i * 3] =
      img[j * width * 3 + i * 3 + 1] =
      img[j * width * 3 + i * 3 + 2] = frame[j * width + i] * 255;
    }
  }
  FILE *fp = fopen("rgb.png", "wb");
  svpng(fp, width, height, img, 0);
  fclose(fp);

  free(frame);
  free(threads_struct);
  free(img);
  return 0;
}
