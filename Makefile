CC = gcc
CFLAGS = -O2
LDFLAGS = -lpthread

CFILES = $(wildcard *.c)
OUTPUTS = $(patsubst %, bin/%, $(CFILES:.c=))

all: bin $(OUTPUTS)

bin:
	mkdir -p bin

bin/%: %.c
	$(CC) $(CFLAGS) $< $(LDFLAGS) -o $@

.PHONY: clean
clean:
	- rm -rf bin/*
