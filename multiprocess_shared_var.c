#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

static int global = 0;

int main() {
  pid_t pid, pid1;
  
  pid = fork();

  if (pid < 0) {
    fprintf(stderr, "Fork failed");
    return 1;
  } else if (pid == 0) {
    pid1 = getpid();
    printf("child: pid = %d\n", pid);
    printf("child: pid1 = %d\n", pid1);
    printf("child: global before = %d\n", global);
    global = 2;
    printf("child: global after = %d\n", global);
  } else {
    pid1 = getpid();
    printf("parent: pid = %d\n", pid);
    printf("parent: pid1 = %d\n", pid1);
    printf("parent: global before = %d\n", global);
    global = 1;
    printf("parent: global after = %d\n", global);
    wait(NULL);
  }

  return 0;
}
